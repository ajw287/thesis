%!TEX root = ../thesis.tex
% ******************************* Thesis Appendix B ********************************

%\newenvironment{conditions*}
%{\par\vspace{\abovedisplayskip}\noindent
%	\tabularx{\columnwidth}{>{$}l<{$} @{${}={}$} >{\raggedright\arraybackslash}X}}
%{\endtabularx\par\vspace{\belowdisplayskip}}

\chapter{Supporting Technical Studies}\label{chpt:nsga_study}%Data on SMO and Nuclear Fuel Management}
\ifpdf
\graphicspath{{./Appendix2/Figures/Raster/}{./Appendix2/Figures/PDF/}{./Appendix2/Figures/}}
\else
\graphicspath{{./Appendix2/Figures/Vector/}{./Appendix2/Figures/}}
\fi

\section{Generating a Sampling Plan that Maximises the Variance of Means}  \label{uniform_mean_sampling_plan}
When training surrogate models on sampled data of a multivariate system, the \gls{maths:climittheorem} explains why non uniform distributions of mean arise, as mentioned in Section~\ref{sect:purerandom}.  In this subsection an attempt is made to control the input values such that the mean is as uniformly varied as possible.  Note that the reason that the mean of uniform variables tends to a Gaussian distribution is that the \textit{density} of solutions is higher in the centre of the distribution.

In this example nine sample values are selected in the range 0.8 to 5.0 in discrete steps of 0.2 to form an \textit{experiment}.  This range and discretisation of the sample set is analogous to the range of discrete enrichments used in Section~\ref{sect:microcore}, where nine values will be sampled per experiment.  Mean value of the samples might affect the system response, for example mean enrichment is known to affect cycle length, the aim of the sampling plan is to create a set of random samples that maximises the variance of the mean.
% \ref{chapt:application}

Just as the Fourier transform converts an arbitrary function into a sum of weighted sine waves, the Weierstrass transform converts a function into a set of weighted Gaussians.  In this section, a \gls{maths:rectangular} is used.  \gls{maths:rectangular} is a function similar to the \glsentrylong{maths:uniform}, but is defined at all points.  The rectangular function, defined as
\begin{equation}
\Pi(x) = \left\{ \begin{array}{rl}
$ 0.0$ &\mbox{if $|x|> 0.5$ } \\
$ 0.5 $ &\mbox{if $|x| = 0.5$} \\
$ 1.0$  &\mbox{if $|x| < 0.5$} 
\end{array} \right.
\end{equation}
will be converted into the expected range by translation and scale
\begin{equation}
f(y) = \Pi\bigg( \frac{y - 2.9}{ 4.4}\bigg)
\end{equation}
thus
\begin{equation}\label{eqn:scaled_boxcar}
f(y) = \left\{ \begin{array}{rl}
$ 0$ &\mbox{ if $y < 0.7$ } \\
$ 0$ &\mbox{ if $y > 5.1$ } \\
$ 0.5 $ &\mbox{if $y = 0.7 \cup y = 5.1$} \\
$ 1$  &\mbox{ if $y > 0.7 \cap y <5.1$} 
\end{array} \right.
\end{equation} 
The aim of the experiment is to to sample them to obtain a net distribution of mean values with maximum range.
Equation \ref{eqn:scaled_boxcar} is transformed using the Weierstrass transform into a sum of Gaussians, the basis function with the largest magnitude are then used as the scaling factors for the constituent basis functions of the distribution.
\begin{align}\label{eqn:weierstrass_transform1}
	\mathbb{W}(x) &= \frac{1}{\sqrt{4 \pi}} \int_{-\infty}^{\infty}f(y) e^{-\frac{(x-y)^2}{4}} dy \\
	\label{eqn:weierstrass_transform2}
	\mathbb{W}(x) &= \frac{1}{\sqrt{4 \pi}} \int_{-\infty}^{\infty}f(x-y) e^{-\frac{(y)^2}{4}} dy \nonumber\\ 
	\mathbb{W}(x) &= \frac{1}{\sqrt{4 \pi}} \Bigg( 0 + \bigg[2xe^{-\frac{y^2}{4}}+ 2e^{-\frac{y^2}{4}}\bigg]_{0.7}^{5.1} + 2 * 0.5 \Bigg)  \nonumber \\
	\mathbb{W}(x) &= \frac{1}{\sqrt{4 \pi}} \Bigg( \bigg[2xe^{-6.5025}+2e^{-6.5025} \bigg]-\bigg[ 2xe^{-0.1225} +2e^{-0.1225}\bigg] + 1
	\bigg) \nonumber   \\
	\mathbb{W}(x) &= 0.50170425075 x - 6.26176906068\nonumber
\end{align}
In Figure \ref{fig:variance_sampling} we can see the results of combining three random distributions, to generate a set of randomly selected experiments with a roughly flat frequency of mean values.  The system uses three distributions in a ratio of $0.3: 0.4: 0.3$, based on the first positive values of the Gaussian domain function.  Triangular random distributions to select the samples for the outer sets of experiments are used, since they are selecting 9 values. The results approximate a Gaussian with a mean at lower and upper intervals ($\mu\approx2.1$ and $\mu\approx 4.7$ respectively). The central distribution is a Gaussian with a standard deviation of one.  The result is a random set that has a similar frequency for a wide range of mean values.  The problem of repeated patterns is most obvious when considering the highest and lowest mean values (in this case 9 samples at 5, or 9 samples at 0.8).  Here there is only one permutation of the design; the next value $\mu=4.9\dot{7}, or \mu = 0.83\dot{3}$, have only 9 permutations; however, this quickly explodes.  As the permutations increase the chance of a repeated a experiment decreases, however the density of experiments decreases with respect to the number of permutation at that mean input vector.  Note from Figure \ref{fig:variance_sampling_repeats}  that a very small number of experiments are repeated ($0.00094\%$) due to the high dimensionality of the problem. 
\begin{figure}[H]
	\centering
	\includegraphics[width=12.1875cm,height=9.1425cm]{\detokenize{190528_max_var_plan.pdf}}
	\caption[A high variance sampling technique]{Combining Gaussian and triangular random distributions to create a set of inputs that has a high mean variance.}
	\label{fig:variance_sampling}
\end{figure}

Consideration of the experiment and number of permutations at, and close to, the limits of the mean of the input vector	 should confirm to the reader that the statistics shown in Figure \ref{fig:variance_sampling_repeats} are representative despite the high level of stochastic noise.

Despite efforts to fight the \gls{maths:climittheorem}, only a small deviation is achieved for nine independent random variables.  For this reason, the approach is not used in further work.  One approach is to accept the \gls{maths:climittheorem} and expect that training sets will have uniform random samples for each input variable and a normally distributed sum, or to use the Sobel sequence and achieve a known space coverage.
%Show the graphs of algorithm statistics and and table of algorithms from JMetal and PISA using google scholar. From First year report...
\begin{figure}[H]
	\centering
	\includegraphics[width=12.1875cm,height=9.1425cm]{test.pdf}%\detokenize{190528_max_var_repeats_1E7.pdf} }%note these are the same file... for some reason latex threw its toys out the pram...
	\caption[Repeated patterns in the sampling plan]{Checking the frequency of repeated sample plans in the combined Gaussian and triangular random distributions.}
	\label{fig:variance_sampling_repeats}
\end{figure}
\section[NSGA 2 Parametrisation Study]{NSGA 2 Parametrisation Study for the 6$\times$6 Microcore} \label{sect:nsga_study}

\subsection{NSGA2 studies in literature}

The author examined work by Deb et al. \cite{deb2002fast} Li and Zhang  \cite{li2009multiobjective} and Latterulo et al,\cite{1}.  %The first paper discusses the usage of NSGAII and parameterisation, whereas the other two use NSGAII in the context that it will be used here, by comparing it with another algorithm.

The parameters from these studies and the Pagmo2 API'S default values are shown in in table \ref{table:nsga2param}
%A maximum population investigated 
%\begin{conditions*}
%gen      & Number of generations to evolve &\\
%cr       & Crossover probability&0.95\\
%$\eta_c$ & Distribution index for crossover&10\\
%m        & Distribution index for crossover&0.01\\
%$\eta_m$ & Distribution index for mutation & 50\\
%seed     & seed used by the internal random number generator &random
%\end{conditions*}
\begin{table} [!htb]%[H]%
	\centering
	\caption{NSGA2 parameters}
	\label{table:nsga2param}
	\begin{tabular}{ c l c c c c }
		\toprule
		\multirow{2}{*}{\textbf{Parameter}} &\multirow{2}{*}{\textbf{Description} } &\multirow{2}{*}{\textbf{Pagmo default}} &  \multicolumn{3}{c}{\textbf{NSGA2 references}}  \\
		& &  &\textbf{\cite{deb2002fast}} &\textbf{\cite{li2009multiobjective}}&\cite{1}\\ \hline
		$P$      & Size of the population& - & 100 &300~$^a$& 100 \\
        $N$      & Number of generations & - & 250 & 500& 3, 12, 16, 20 \\
        cr       & Crossover probability&0.95& 0.9 & 0.1-1.0& - \\
        %$\eta_c$ & Distribution index for crossover&10& 20 & 20& 20 \\
        m        & Mutation probability&0.01& $1/n_{vars}$ & - & $1/n_{vars}$\\
        %$\eta_m$ & Distribution index for mutation & 50& 100 & 10 & 100 \\
        seed     & Seed of internal RNG &clock& - & - & - \\
		\hline
	\end{tabular}
	\footnotesize{$^{a}~595$ used for three objectives}
\end{table}
A fully exhaustive study of the parameter variation with respect to every value was not possible.  However, based on an investigation of the parameter values used in the other scientific literature. % (Deb, \cite{deb2002fast}, Li and Zhang \cite{li2009multiobjective} and Lattarulo et al \cite{1} ).
A simplified `compass search'  style method was used to maximise the mean hypervolume parameter over a number of runs with adjacent seed values.  Although the origin of compass search is unclear, an early use in digital computers was by Fermi and Metropolis to establish scattering cross sections (see Anderson et al, \cite{anderson1955scattering} found in Kolda et al\cite{kolda2003optimization})
%Due to the high computational demand of running Monte Carlo neutron transport for the microcore, it was not possible to carry out a fully exhaustive study, this is similar to Lattarulo et al \cite{1}.

In order to compare the efficacy of a multiobjective optimisation runs the hypervolume parameter is used.  Nowak et al. \cite{nowak2014empirical} attribute hypervolume parametrisation of multiobjective optimisation to Zitzler et al. \cite{zitzler1998multiobjective}.

Firstly, the population size, $P$ and number of generations, $N$ were identified as the most computationally expensive variables to investigate and were varied first with the other variables set to the Pagmo default values.  Once suitable values of this had been established from this study the other variables were tested for the chosen population and number of generations.
\begin{table} [!htb]%[H]%
	\centering
	\caption{Parameters used in this study}
	\label{table:studyValues}
	\begin{tabular}{ c l  }
		\textbf{Parameter} &\textbf{Candidate Values}  \\ \hline
		$N$      & 1 - 100 \\
		$P$      & 20, 32, 40, 60, 80, 100, 120 \\
		cr       & 0.7, 0.8, 0.9, 0.95, 0.9999\\
%		$\eta_c$ & 5, 10, 15, 20 \\
		m        & 0.001, 0.01, 0.05, 0.1\\
%		$\eta_m$ & 10,50,100 \\
		seed(selection):& 3453412-6\\
		seed(population):&3453213-7\\
		\hline
	\end{tabular}
\end{table}
\subsection{Number of generations}
The first test ran a population of 20 individuals, for 100 generations.  At such a small population size, the system is unlikely to converge to the global optimum, however the aim of the experiment is to establish how quickly the algorithm converges when acting on this problem.  For this reason Figure \ref{fig:nsga2GenHyper} shows the percentage increase of the hypervolume parameter as the metric for improvement while Figure \ref{fig:nsga2GenHyperAbs} shows the actual values.

The hypervolumes plotted in figure \ref{fig:nsga2GenHyper}, show that for this problem, the NSGA2 algorithm running for 40 generations, has an average hypervolume indicator that is $97.735\%$ to $3 d.p.$ of the maximum hypervolume achieved after a further 60 generations, furthermore figure \ref{fig:nsga2GenHyperAbs} shows that there is a full correlation between low rate of convergence runs and poor overall performance, which is significant since the end goal of an optimisation problem is simply the absolute best solution found, rather than the mean solution found.  These results are used to justify the investigation of subsequent optimisation problems for at least 40 generations. 
\begin{figure}[!htb]
	\centering
%	\vspace{-0.35cm}
	\includegraphics[scale=1.0]{Figure1-gen-hypervolumes.pdf}
	\caption[Relative hypervolume indicator plot for NSGA2]{Relative hypervolume indicator plot for NSGA2 over generations.}   
	\label{fig:nsga2GenHyper}
\end{figure}%(data: \texttt{pygmo\_micro.py}, graph: \texttt{plot\_hypervolumes.py}
\begin{figure}[!htb]
	\centering
	%	\vspace{-0.35cm}
	\includegraphics[scale=1.0]{Figure2-gen-hypervolumes-abs4.pdf}
	\caption[Absolute hypervolume indicator plot for NSGA2]{Absolute hypervolume indicator plot for NSGA2 over generations.}
	\label{fig:nsga2GenHyperAbs}
\end{figure}
%\begin{figure}
%	\centering
%	\begin{subfigure}[b]{.5\linewidth}
%		\includegraphics[width=\textwidth]{Figure1-gen-hypervolumes.pdf}
%		\caption{Relative hypervolume indicator plot for NSGA2 over generations N=20 \texttt{pygmo\_micro.py}}   
%		\label{fig:nsga2_gen_hyper}  
%	\end{subfigure}\hfill        
%	\begin{subfigure}[b]{.5\linewidth}
%		\includegraphics[width=\textwidth]{Figure2-gen-hypervolumes-abs.pdf}
%		\caption{Absolute hypervolume indicator plot for NSGA2 over generations N=20 \texttt{pygmo\_micro.py}}   
%		\label{fig:nsga2_gen_hyper-abs}
%	\end{subfigure}
%	\caption{Optimal number of generations for NSGA2 on microcore}
%	\label{fig:generations}
%\end{figure}
\subsection{Populations size}
The next test investigates the size of the population, this was tested for 40 generations for a population size ($pop$) over a range of 20 to 120 individuals, due to constraints of the NSGA2 algorithm, only population numbers divisible by 4 are possible, so the values chosen were: 20,32,40,60,80,100,120.  Although 120 individuals is still less than that of Li et al. \cite{li2009multiobjective}, it is similar to other studies and represents a vast improvement in terms of number of total simulations compared to other studies.

Population size has the primary effect of decreasing the likelihood of the final solution being in a local optima.  This means that while the actual mean hypervolume indicator achieved is important the variance of the results are also significant.

For this reason the results shown in Figure \ref{fig:nsga2PopHyper} plot the average value of hypervolume  relative to the overall average on the dashed line, then two volumes represent the mean value above the line and below the line and the max values achieved.
\begin{figure}[!htb]
	\centering
	%	\vspace{-0.35cm}
	\includegraphics[scale=0.65]{Figure3-pop-hypervolumes-dev.pdf}
	\caption[Hypervolume indicator vs population size]{Relative final hypervolume indicator vs population size \ref{fig:nsga2GenHyperAbs}.}   
	\label{fig:nsga2PopHyper}
\end{figure}%(data:  \texttt{pygmo\_micro.py}, graph: \texttt{plot\_hypervolumes\_vs\_pop.py})

From this graph it can be seen that there is a positive correlation between the number of individuals in the population, $P$, and the value of the final hypervolume.  However, the number of individuals has a large effect on the final computational budget and the gains achieved decrease as the population size goes up.  The effect of population size increases is very small, of the order of $0.1\%$ in this problem.   For this reason, a population size of 50 is chosen for further experiments.
%Hypervolume vs population size x generations.

\subsection{Population \texorpdfstring{$P$}{P} and generations \texorpdfstring{$N$}{N}}
It should be noted that for each of the parameters so far there have been two implicit considerations.  Namely that the use of more generations and larger populations cost computational time, the aim of this study is to establish the lowest cost that is effective with the NSGA2 algorithm applied to the kind of problems that are being investigated.  Although the results of the previous section show that a larger population produces a larger expected hyperparameter, the increase is very small, with this in mind it becomes unfair to compare NSGA2 with a surrogate model in terms of cost, when using a very large populations size for the direct NSGA2 simulations, since we know that the expected gains will be small and the cost large.

Furthermore, due to the stability of the Monte Carlo simulations $\approx0.1\%$ of simulations fail.   This means that as $N$ and $P$ increase the number of simulations required increases at $N\times P$.  Eventually it becomes cumbersome to run simulations that must be repeatedly restarted.

The subsequent variables do not have these implicit costs, and so can be varied without `knock on' effects of the choices.

\subsection{Crossover \texorpdfstring{$cr$}{cr} and mutation rate \texorpdfstring{$m$}{m}}
From the study of crossover values it can be seen that lower crossover values result in a larger range of hypervolumes, this is attributed to the low rate of crossover causing higher specialisation in the optimisation algorithm (Figure \ref{fig:nsga2CrossHyper})).  Based on this result, the default value of $0.095$ is selected for use in the rest of the  study, since this gives a low range of results in the test, while still giving a large value for average evolved hypervolume.

Figure \ref{fig:nsga2MuteHyper} shows the effect that mutation rate has upon the average hypervolume evolved from the study.  While there is a clear correlation between the mutation rate and the hypervolume evolved, and the range of the hypervolumes evolved reduces with the increase in mutation rate, however it is about ten times smaller than the total range of hypervolume evolved.  Due to the small effect, the default mutation rate of $1\%$ is chosen for moving forwards,  this is considered acceptable, since the range of hypervolume change due to mutation is less than $0.1\%$
\begin{figure}[!htb]
	\centering
	%	\vspace{-0.35cm}
	\includegraphics[scale=0.65]{new_crossovers_rel2.pdf}
	\caption[Hypervolume indicator vs crossover]{Hypervolume indicator vs Crossover rate.}   
	\label{fig:nsga2CrossHyper}
\end{figure}% (data:  \texttt{pygmo\_micro.py}, graph: \texttt{plot\_hypervolumes.py})
\begin{figure}[H]
	\centering
	%	\vspace{-0.35cm}
	\includegraphics[scale=0.65]{mutation_2.pdf}
	\caption[Hypervolume indicator vs mutation]{Hypervolume indicator vs mutation rate.}   
	\label{fig:nsga2MuteHyper}
\end{figure}%(data:  \texttt{pygmo\_micro.py}, graph: \texttt{plot\_hypervolumes.py})
%\subsection{Crossover and mutation distribution parameters}

\subsection{Review}
The conclusion of this section is that the NSGA2 algorithm is robust to a wide variety of hyperparameter changes.  That is to say that it is impossible to argue that the algorithm is particularly tuned to the \gls{terms:smo} and detuned to the \gls{terms:dso}.  This is significant because it allows the evaluation side by side evaluation of the \gls{terms:smo} and \gls{terms:dso}.  The NSGA2 algorithm was expressly designed to be robust to a wide variety of problems \cite{deb2002fast}.

Another aim this study has been to find the parameters that represent the fairest set of parameters for use of the NSGA2 algorithm as a control optimisation method, to compare \gls{terms:dso} with \gls{terms:smo}.  This puts the author into a state of moral hazard for parameters that have a computational cost associated with them, \textit{e.g.} number of generations and population size, where a greater value yields better hypervolume performance, but at an increased computational cost.  It would be legitimate but misleading to argue that the NSGA2 algorithm requires exceptionally large populations or extended number of generations,  the argument could then easily be made that surrogate model methods represent a huge improvement on this classical algorithm.

The opposite position has been taken, to select parameters for the NSGA2 algorithm parameters that represent a realistic choice.  This has been based on asking what an engineer might choose were they working with a limited budget for computational cost.  

This basic `compass search' in four dimensions, has required more than 236, 000 CPU hours (equivalent to  twenty six years, and eight months of time on a single CPU time) and more than four months of continuous calender time queueing jobs on the Cambridge university HPC (ranked 75 in world HPCs at the time of this study \cite{top500camb}).  The choice to act frugally towards the computational budget immediately makes proving the advantages of \gls{terms:smo} more difficult, but hopefully lends credibility to the conclusion.

Crucially the study has shown that there is only a small increase in performance for NSGA2 on this problem between a population of forty and a population of one hundred and twenty.  In this range, for a given computational budget, it is equivalent to run more simulations on a smaller population then to optimise with larger populations.

The conclusion of this study is that most of the hyperparameters make very little difference to the results in terms of average hypervolume of the optimisation for this problem.

Table \ref{table:nsgaSummaryValues} is used to recapitulate the selected values of the parameters used in this study. 

\begin{table} [!htb]%[H]%
	\centering
	\caption{Optimal parameters used in further work}
	\label{table:nsgaSummaryValues}
	\begin{tabular}{ c l  }
		\toprule
		\textbf{Parameter} &\textbf{Preferred Value(s)}  \\ \midrule
		$N$      & $\geqslant60$ \\
		$P$      & $\geqslant40$\\
		cr       & $0.95$\\
		%$\eta_c$ & $5, 10, 15, 20$ \\
		m        & $0.01$\\
		%$\eta_m$ & $10,50,100$ \\
		\hline
	\end{tabular}
\end{table}
\section{Kolmogorov-Smirnov Test}\label{app-kolmogorov}
A Kolmogorov-Smirnov (K-S) Test has been carried out for the \gls{terms:dso} and \gls{terms:smo} \glspl{terms:ndf} in section \ref{sec:experiment2}.  In order to use the K-S Test, modifications are made to establish a bivariate investigation.  This was carried out according Peacock \cite{peacock1983two} and investigated and shown to be applicable for practical purposes without assumptions about the distribution by Fasano and Franceschini \cite{fasano1987multidimensional}.
 
\begin{table} [!htb]%[H]%
	\centering
	%	\vspace{-0.75cm}
	\caption[K-S Test for DSO and SMO]{K-S Test for \gls{terms:dso} and \gls{terms:smo}, against the rest of the runs and ensemble of runs vs categories.}
	\label{table:smo-dso-k-s-test}
	\begin{tabular}{ l c c }
		\toprule
		\multirow{2}{*}{\textbf{K-S Test}}&\multicolumn{1}{c}{\textbf{DSO} }&\multicolumn{1}{c}{\textbf{SMO}}\\
		& p-value            & p-value\\
		\midrule 
		run vs ensemble
		&  $0.260419$ &  $0.570637$  \\
		&  $0.995971$ &  $0.772541$  \\
		&  $0.004182$ &  $0.854263$  \\
		&  $0.536309$ &  $0.737902$  \\
		&  $0.619042$ &  $0.863347$  \\
		\textbf{mean} &  $\mathbf{0.4831846}$ & $\mathbf{0.759738}$ \\
		\textbf{mean DSO vs SMO:} & \multicolumn{2}{c}{$\mathbf{ 0.3170356}$} \\
		\hline
	\end{tabular}
\end{table}
%DSO Self K-Test
%(0.26041947499612506, 0.184375)
%(0.9959708455254134, 0.07500000000000001)
%(0.0041828154204516495, 0.31875)
%(0.5363093555002926, 0.14687499999999998)
%(0.6190423176761998, 0.1375)
%SMO Self K-Test
%(0.570636765980633, 0.21794871794871795)
%(0.7725409195324857, 0.1669928245270711)
%(0.8542627539251966, 0.1532941943900848)
%(0.7379018846531877, 0.1722113502935421)
%(0.8633477444974476, 0.17088607594936708)
%SMO vs DSO K-S Test
%(0.03321527731475755, 0.1876063829787234)
%(0.6794295636707794, 0.19)
%(0.2234854327996715, 0.24535714285714288)
%(0.2058873663769917, 0.25035714285714283)
%(0.4544034275388787, 0.20095238095238097)
%(0.30579263458542244, 0.26249999999999996)

\subsection{Review}
The K-S Test is introduced in Section~\ref{section:statistical-tests}, was expected to be a useful measure of the difference of statistical results. Table \ref{table:smo-dso-k-s-test} shows the `p-value' output of the K-S Test, this is the likelihood that the two sets of statistics were generated by the same distribution, \textit{regardless of the distribution}.  There is significantly more correlation between the \gls{terms:ndf} sets with multiple runs than between the \gls{terms:dso} and \gls{terms:smo}.  However, in this application, the correlation is too low to be a useful test, as noted by Fasano and Francheschini \cite{fasano1987multidimensional}, significant error is evolved when the correlation is low.  Also, the results show a high variance.

The K-S test also does not apply to categorical systems with a high likelihood of repeated solutions \cite{niststatshandbook}.  Although the search space for this experiment is $\approx5.12\times 10^11$ combinations of inputs, the true \gls{terms:ndf} of this space is much smaller, and the likelihood of repeated values in this space is potentially high.  This is noted to be a problem by  \cite[p. 584]{LehmannEStatistics} and is proposed as an explanation for the low correlation of iterations of the same method, in the results.

\section{PPF or pin power variance}\label{ppf-variance}
In an initial version of Chapter~\ref{chap:deep_learning}, experiment 3 (p. \pageref{ex3-burnup}),  pin power variance was used as an objective variable.  Variance has reduced error  when calculated using a Monte Carlo method compared to \gls{nuclear:ppf} which has an error dependent on the peak power pin.  \Gls{nuclear:ppf} is in fact a proxy variable used in optimisation to represent \gls{nuclear:dnbr},  since the plant can be operated to a point where the peak pin departs from nucleate boiling (with a safety margin).  Other reasons why a nuclear engineer might be interested in the variance of the pin powers would be if the uniformity of the coolant outlet temperature.  However due to the ambiguity of the value of the pin power variance, it is presented in the appendix rather than the chapter.

An initial investigation of pin power variance from indicates that \gls{nuclear:ppf} and variance in random \glspl{nuclear:lp} had a broad correlation, while at the \gls{terms:ndf} the correlation was very good, as shown in Figure \ref{fig:ppf-var-obj-ppf}.  This lead to the study of pin power variance as an objective.  When pin power variance is used as an objective, as shown in Figure \ref{fig:ppf-var-obj-var}, the resulting pin power variance is on the low side of the correlation.  From this data we can see that \gls{nuclear:ppf} correlates well with variance at the \gls{terms:ndf}, and the \gls{terms:ndf} of the variance study implies relatively low \gls{nuclear:ppf}. 

Following the same template as the other studies in Chapter~\ref{chap:deep_learning}, it can be seen that the \gls{terms:mlp} surrogate model produced creates solutions with significantly larger EOC burnup at over 17 MWd/kgU.
%An initial investigation of pin power variance from experiment 2 showed that \gls{nuclear:ppf} and variance in random \glspl{nuclear:lp} had a broad correlation, while at the \gls{terms:ndf} the correlation was very good, as shown in Figure \ref{fig:ppf-var-obj-ppf}.  It was incorrectly believed that this correlation allowed the variance to be used as an objective.  However, when objective is variance, as shown in Figure \ref{fig:ppf-var-obj-var}, it is obvious that this is false.  Here we can see that \gls{nuclear:ppf} is not a good indicator of the variance, but the variance is still mid range.

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[b]{0.49\textwidth}
		\includegraphics[width=0.95\linewidth]{\detokenize{ppf-var-obj-ppf.pdf}}
		\caption{Correlation based on data from Experiment 2}
		\label{fig:ppf-var-obj-var}   %seed $= 3454321$, 
	\end{subfigure}             
	\begin{subfigure}[b]{0.49\textwidth}
		%\includegraphics[width=1.0\linewidth]{\detokenize{figure-5-12b-graph.pdf}}
		\includegraphics[width=1.0\linewidth]{\detokenize{ppf-var-obj-var.pdf}}
		\caption{Correlation based on data from Experiment 3}
		\label{fig:ppf-var-obj-ppf}%seed $=3453412$, 
	\end{subfigure}         
	\caption[PPF and pin power variance]{Correlation of \gls{nuclear:ppf} and pin power variance.}\label{fig:ppf-var}
\end{figure}

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[b]{0.49\textwidth}
		\includegraphics[width=0.95\linewidth]{\detokenize{figure-5-12-graph.pdf}}
		\caption{DSO,  $\sim4000$ cpu hrs, (hpc)}
		\label{fig:ex3-dso-var-cycle-result}   %seed $= 3454321$, 
	\end{subfigure}             
	\begin{subfigure}[b]{0.49\textwidth}
		%\includegraphics[width=1.0\linewidth]{\detokenize{figure-5-12b-graph.pdf}}
		\includegraphics[width=1.0\linewidth]{\detokenize{bu_run-graph.pdf}}
		\caption{MLP SMO,  $<0.0019$ cpu hrs, (laptop)}
		\label{fig:ex3-smo-var-cycle-result}%seed $=3453412$, 
	\end{subfigure}         
	\caption[DSO and SMO results]{Examples for \gls{terms:dso} and \gls{terms:smo} initial and final population results.}\label{fig:ex3-dso-smo-result2}
\end{figure}

\begin{figure}[!htb]
	\begin{subfigure}[b]{\textwidth}
		\centering
		\includegraphics[scale=0.85]{\detokenize{surrogate_core_designs_burnup_3.pdf}}
		\caption[Sample SMO results]{Performant \gls{nuclear:lp} arrangements generated by a \gls{terms:cnn} \gls{terms:smo} (seed=$3454312$)}   
		\label{fig:microcore-smo-results-ex32}
	\end{subfigure}             %, \texttt{pygmo\_micro.py}
	\begin{subfigure}[b]{1.0\textwidth}
		\centering
		\includegraphics[scale=0.85]{\detokenize{burnup_core_designs_v3.pdf}}
		\caption[Example DSO results]{\gls{terms:dso} \gls{nuclear:lp} arrangements generated for the bottom righthand quadrant of the microcore (seed=$3454321$)}   
		\label{fig:microcore-dso-results-ex23}
	\end{subfigure}    %, \texttt{\detokenize{nsga_burn/pygmo_micro.py} }     
	\caption[DSO and SMO burnup results]{Examples for \gls{terms:dso} and \gls{terms:smo} \glspl{nuclear:lp}, maximising burnup and minimising pin power variance }\label{fig:microcore-results-exp32}
\end{figure}

\begin{figure}[!htb]
	%\centering
	%	\vspace{-0.35cm}
	\includegraphics[scale=1.0]{\detokenize{burnup-test-compare-graph.pdf}}
	\caption[SMO results in Serpent vs DSO for burnup]{\gls{terms:ndf} \gls{nuclear:lp} for the \gls{terms:cnn} \gls{terms:smo} simulated in serpent.}   
	\label{fig:ex3-smo-results2}
	%	%	\vspace{-0.75cm}
\end{figure}%, \texttt{pygmo\_micro\_array.py}

\section{Required Neutron Population for Fission Matrix}\label{sec:neutron-pop-fmtx}
In order to establish the correct number of neutrons to be used to generate fission matrices, the stochastic noise, $\nu_n$ of the fission matrix is estimated by using equation \ref{eqn:fmtx-noise}.  This assumes that the scattering is isotropic.  The results for simulations carried out with a number of different population sizes are shown graphically in Figure \ref{fig:fmtx-diagonal-sim}.
\begin{equation}
\nu_n n = | abs(F - F^{T}) |
\end{equation}\label{eqn:fmtx-noise}

\begin{figure}[!htb]
	\centering
	\includegraphics[scale=1.0]{\detokenize{Population-vs-error-2-graph.pdf}}
	\caption[Difference between fission matrix and its transpose]{Mean difference between fission matrix and its transpose for a number of simulations} \label{fig:fmtx-diagonal-sim}
\end{figure}
\clearpage