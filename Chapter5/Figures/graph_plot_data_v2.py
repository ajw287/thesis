import sys, os
import pickle
import matplotlib.pyplot as plt
import numpy as np
import pareto
from decimal import Decimal

import matplotlib.font_manager as font_manager
# Dark2 is found at: https://matplotlib.org/tutorials/colors/colormaps.html done badly here! TODO:fix
dark2 =["#1b9e77", "#d95f02", "#7570b3", "#e7298a", "#66a61e", "#e6ab02", "#a6761d", "#666666", "#c61e11"]
fig_font = {'fontname':'Liberation Serif'}
font = font_manager.FontProperties(family='Liberation Serif')

class plotDataOut:
    pass

def num_last_chars( x):
    return(x[-4:])
def feq(a,b):
    if abs(a-b)<0.0001:
        return True
    else:
        return False

def up_round(x, base=0.2):
    if feq(Decimal(x) % Decimal(base), Decimal(0.2) ):
        return x
    elif feq(Decimal(x) % Decimal(base), Decimal(0) ):
        return round(x,1)
    elif feq(Decimal(x) % Decimal(base), Decimal(0.1) ):
        return round(x+0.1, 1)
    else:
        print ("Error invalid rounding value..." +str(x))
        print (str(Decimal(x) % Decimal(base)) )
        print(str(Decimal(0.1)))
        print(Decimal(Decimal(x) % Decimal(base)) == Decimal(0.1))
        exit()
def get_hot_pin_ppf( d):
    full = np.block([[d[0], d[1], d[2]],
                     [d[3], d[4], d[5]],
                     [d[6], d[7], d[8]] ])
    #print(np.shape(full))
    hot_pin = np.unravel_index(full.argmax(), full.shape)
    max = np.max(full)
    full[full == 0.0] = np.nan
    mean_no_zero = np.nanmean(full.ravel())
    ppf = max / mean_no_zero
    #print(str(mean_no_zero))
    #print(str(ppf))
    return (list(hot_pin) + [ppf])

    # takes a list of assembly powers and finds
    # the ppf and the position of the hot pin7

def main():
######################## DSO ###########################
    # Plot
    fig, ax = plt.subplots()
    first = 0.0
    dso_pickles = ["../dso_pickle/s3454320_plot_data.pickle", "../dso_pickle/s3454321_plot_data.pickle", "../dso_pickle/s3454323_plot_data.pickle"]
    for dso_pickle in dso_pickles:
        # plot the ndf of the DSO
        with open(dso_pickle,'rb') as pickle_file:
            plotdatadso = pickle.load(pickle_file, encoding='latin1')
            final_outputs_dso = sorted(plotdatadso.final_outputs, key=lambda k: [k[1], k[0]])
            dso_x = [row[0]for row in final_outputs_dso]#[dist(row[0], row[1]) for row in final_outputs_dso]
            dso_y = [row[1]for row in final_outputs_dso]#[row[2] for row in final_outputs_dso]
            print(plotdatadso.final_outputs)
        if first == 0.0:
            plt.step(dso_x, dso_y, '4-', color=dark2[-1], alpha=0.9, where='pre', label='NDF of DSO runs')
        else:
            plt.step(dso_x, dso_y, '4-', color=dark2[-1], alpha=0.9-first, where='pre')
        first +=0.2

    #################  THIS IS THE BIT WHERE I LOAD THE SERPENT DATA   #############
    first = 0.0
#    smo_dirs = ["../process_smo_pareto_ex1/runs/", "../process_smo_pareto_ex1/s3453414/"]
    smo_dirs = ["../process_smo_pareto_ex1/s3453412/", "../process_smo_pareto_ex1/s3453413/", "../process_smo_pareto_ex1/s3453414/"]
    for smo_dir in smo_dirs:
        full_dir_list = os.listdir(smo_dir)
        file_list = [f for f in full_dir_list if f.endswith(".pickle")]
        # extract the data from the files.
        y_sim_vals = []
        x_vals_in = []
        outputs_smo = []
        for i,filename in enumerate(sorted(file_list, key = num_last_chars) ):
            with open(smo_dir+filename,'rb') as pickle_file:
                var = pickle.load(pickle_file, encoding='latin1')
                results = get_hot_pin_ppf(var.detector_data)
                ppf = results[-1] # last result is ppf
                outputs_smo.append([np.mean(var.enrichments), ppf])
                y_sim_vals.append(ppf)
                enrichments = [up_round(x) for x in var.enrichments]
                x_vals_in.append(np.mean(enrichments))
        final_outputs_smo = sorted(outputs_smo, key=lambda k: [k[1], k[0]])
        ndf_list, rest_to_discard = list(pareto.pareto_front(final_outputs_smo))
        smo_x = [row[0] for row in ndf_list]#[dist(row[0], row[1]) for row in final_outputs_dso]  #0.2 is a correction for rounding up in the simulations
        smo_y = [row[1]for row in ndf_list]#[row[2] for row in final_outputs_dso]

        #ax.scatter(smo_x, smo_y,  c=dark2[2], alpha=0.8)
        if first == 0.0:
            plt.step(smo_x, smo_y, '2-', color=dark2[2], alpha=1.0, where='post', label='NDF of SMO runs') #'o-',
        else:
            plt.step(smo_x, smo_y, '2-', color=dark2[2], alpha=1.0-first, where='post')#'o-',
        first+=0.2
    ###################    Bells and whistles ##################
    ax.set_title('Comparison of NDFs from SMO and DSO runs, evaluated in Serpent', **fig_font)
    ax.set_ylabel('PPF', **fig_font)
    ax.set_xlabel('Enrichment (w/o U235)', **fig_font)


    # horrible re-order code from: https://stackoverflow.com/questions/22263807/how-is-order-of-items-in-matplotlib-legend-determined
    handles, labels = plt.gca().get_legend_handles_labels()
    #order = [1,2,3,0]
    #ax.legend([handles[idx] for idx in order],[labels[idx] for idx in order],loc=2, prop=font)
    ax.legend(loc=1, prop=font)
    plt.show()
    fig.savefig('figure-5-6-graph.svg')


if __name__ == "__main__":
    main()
