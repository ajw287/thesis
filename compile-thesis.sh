#!/bin/bash
# A script to compile the PhD Thesis - Krishna Kumar 
# Distributed under GPLv2.0 License

compile="compile";
clean="clean";
filename=$2
if test -z "$2"
then
if [ $1 = $clean ]; then
	echo "Cleaning please wait ..."
        rm -f *.1i
        rm -f *.2i
        rm -f *.3i
        rm -f *.1o
        rm -f *.2o
        rm -f *.3o
        rm -f *.ist
	rm -f *~
	rm -rf *.aux
	rm -rf *.bbl
	rm -rf *.blg
	rm -rf *.d
	rm -rf *.fls
	rm -rf *.ilg
	rm -rf *.ind
	rm -rf *.toc*
	rm -rf *.lot*
	rm -rf *.lof*
	rm -rf *.log
	rm -rf *.idx
	rm -rf *.out*
	rm -rf *.nlo
	rm -rf *.nls
	rm -rf $filename.pdf
	rm -rf $filename.ps
	rm -rf $filename.dvi
	rm -rf *#* 
	echo "Cleaning complete!"
	exit
elif [ $1 = $compile ]; then
#        makeglossaries $filename
        echo "Compiling your PhD Thesis...please wait...!"
        pdflatex -interaction=nonstopmode $filename.tex
        #makeindex -s $.ist -o <yourfile>.acr <yourfile>.acn
        bibtex $filename.aux
        makeindex $filename.aux
        makeindex $filename.idx
        makeglossaries $filename
#       makeindex $filename.nlo -s nomencl.ist -o $filename.nls
        pdflatex -interaction=nonstopmode $filename.tex
#       makeindex $filename.nlo -s nomencl.ist -o $filename.nls
        makeglossaries $filename
        pdflatex -interaction=nonstopmode $filename.tex
        echo "Success!"
        exit
else
	echo "Shell script for compiling the PhD Thesis"
	echo "Usage: sh ./compile-thesis.sh [OPTIONS] [filename]"
	echo "[option]  compile: Compiles the PhD Thesis"
	echo "[option]  clean: removes temporary files no filename required"
	exit
fi
fi

filename=$2;

if [ $1 = $clean ]; then
	echo "Cleaning please wait ..."
	rm -f *~
	rm -f *.1i
	rm -f *.2i
	rm -f *.1o
	rm -f *.2o
	rm -rf *.aux
	rm -rf *.bbl
	rm -rf *.blg
	rm -rf *.d
	rm -rf *.fls
	rm -rf *.ilg
	rm -rf *.ind
	rm -rf *.toc*
	rm -rf *.lot*
	rm -rf *.lof*
	rm -rf *.log
	rm -rf *.idx
	rm -rf *.out*
	rm -rf *.nlo
	rm -rf *.nls
	rm -rf $filename.pdf
	rm -rf $filename.ps
	rm -rf $filename.dvi
	rm -rf *#* 
	echo "Cleaning complete!"
	exit
elif [ $1 = $compile ]; then
	makeglossaries $filename
	echo "Compiling your PhD Thesis...please wait...!"
	pdflatex -interaction=nonstopmode $filename.tex
	#makeindex -s $.ist -o <yourfile>.acr <yourfile>.acn
	bibtex $filename.aux 	
	makeindex $filename.aux
	makeindex $filename.idx
#	makeindex $filename.nlo -s nomencl.ist -o $filename.nls
	pdflatex -interaction=nonstopmode $filename.tex
#	makeindex $filename.nlo -s nomencl.ist -o $filename.nls
	makeglossaries $filename	
	pdflatex -interaction=nonstopmode $filename.tex
	echo "Success!"
	exit
fi


if test -z "$3"
then
	exit
fi
