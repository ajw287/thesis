import sys
#import random as rand
import numpy as np
import random
import matplotlib.pyplot as plt

import matplotlib.font_manager as font_manager
#import matplotlib.font_manager as fm
fig_font = {'fontname':'Liberation Serif'}
font = font_manager.FontProperties(family='Liberation Serif')


def print2Dlist(twoDlist):
    print ("[")
    for line in twoDlist:
        print (line)
    print("]")

def pareto_front(twoDlist):
# from:https://math.stackexchange.com/questions/101125/how-to-compute-the-pareto-frontier-intuitively-speaking
#
# We say that an alternative A dominates B if A outscores B
# regardless of the tradeoff between value and cost that is,
# if A is both better and cheaper than B.
#
#Algorithm A:
#1. Let i:=1
#2. Add Ai to the Pareto frontier.
#3. Find smallest j>i such that value(Aj)>value(Ai)
#4. If no such j exists, stop. Otherwise let i:=j and repeat from step 2.
    i = 1
    for line in twoDlist:
        line.append(i)
        i+=1
    sortedList = sorted(twoDlist,key=lambda x:(x[0],x[1]))
    y = sortedList[0][1]
    #sortedList[0].append("pareto")
    for line in sortedList:
        if line[1] > y:
            line.append("dominated")
        else:
            line.append("pareto")
            y = line[1]
    pareto = []
    dominated = []
    for line in sortedList:
        if (line[3] == "pareto"):
            pareto.append(line)
        else:
            dominated.append(line)#[:2])
    print("pareto:")
    print2Dlist(pareto)
    #print2Dlist(dominated)
    return pareto, dominated

epsilon = 2.15
low = 0.0
high= 3.0
size= 45
np.random.seed(int(sys.argv[1])) # 6 is ok...
#randoms = [[random.uniform(low, high),random.uniform(low, high)] for i in xrange(size)]
randoms = []
for i in range(size):
    x_vals = np.random.normal(loc=5.0, scale=3.0, size=None)
    y_vals = np.random.normal(loc=5.0, scale=3.0, size=None)
#    xvals =[x_vals - y_vals * y_vals]
    xvals = x_vals - np.sqrt(y_vals)
    yvals = y_vals - np.sqrt(x_vals)
    randoms.append([[xvals],[yvals]])
pareto, dominated = pareto_front (randoms)
plt.xkcd()
ax = plt.gca()
x = [row[1] for row in pareto]
y = [row[0] for row in pareto]
#plt.plot(x,y, color='r', alpha=.65, marker='x', linestyle='-')
plt.step(x, y, color='r', alpha=0.65, where='pre', marker='x', linestyle='-', label="first NDF")
for x_t, y_t in zip(x[-9:-2],y[-9:-2]):
    print(x_t[0])
    print(y_t[0])
    x_start = x_t[0]
    y_start = y_t[0]
    ax.annotate("",
                xy=(x_start+epsilon, y_start+epsilon), xycoords='data',
                xytext=(x_start, y_start), textcoords='data',
                arrowprops=dict(arrowstyle="->", color="0.3",
                                shrinkA=5, shrinkB=5,
                                patchA=None, patchB=None,
                                #connectionstyle="arc3,rad=-0.3",
                                ),
                )
x_start=x[-3][0]
y_start=y[-3][0]

# less good ndf
x = [row[1][0]+1+np.random.normal(loc=1.0, scale=0.5) for row in pareto[4:-3]]
y = [row[0][0]+1+np.random.normal(loc=1.0, scale=0.5) for row in pareto[4:-3]]

y[-1] += 1.1
x[-1] += 0.75
x[-2] += 2.75
y[-2] += 0.85
x[-3] += 0.75
x[1]  += 0.50
y[1]  += 0.32
x[0]  += 2.25
y[0]  += 1.15

x = [ ex - 2 for  ex in x]
y = [why - 2 for why in y]
plt.step(x, y, color='g', alpha=0.65, where='pre', marker='x', linestyle='-', label="second NDF")


#ax.annotate("",
#            xy=(x[-3], y[-3]), xycoords='data',
#            xytext=(x[-3]+1., y[-3]+1.), textcoords='data',
#            arrowprops=dict(arrowstyle="->", color="0.5",
#                            shrinkA=5, shrinkB=5,
#                            patchA=None, patchB=None,
#                            connectionstyle="arc3,rad=0.3",
#                            ),
#            )


x = [row[1][0]+epsilon for row in pareto]
y = [row[0][0]+epsilon for row in pareto]
#plt.plot(x,y, color='r', alpha=.65, marker='x', linestyle='-')
plt.step(x, y, color='black', alpha=0.35, where='pre', marker='x', linestyle='--', label="first NDF offset")

ex = 6.6
why = 0.1
ax.annotate("",
            xy=(ex, why), xycoords='data',
            xytext=(ex+1., why+4.), textcoords='data',
            arrowprops=dict(arrowstyle="->", color="0.5",
                            shrinkA=5, shrinkB=5,
                            patchA=None, patchB=None,
                            connectionstyle="arc3,rad=-0.3",
                            ),
            )

t = ax.text(ex+1., why+4., "offset by ε intersects\n with second NDF", ha="center", va="center", size=15, **fig_font)

plt.ylabel('objective y (to be minimised)', **fig_font)
plt.title("Demonstration of ε-indicator:", **fig_font)
plt.xlabel('objective x (to be minimised)', **fig_font)
#prop = font_manager.FontProperties(fname='Liberation Serif', size=16)
#for text in ax.texts:
#    text.set_fontproperties(prop)

print(x_start)
print(y_start)
print(x[-3])
print(y[-3])
#ax.annotate('ε',
#            xy=(x_start+epsilon, y_start+epsilon), xycoords='data',
#            xytext=(x_start, y_start), textcoords='offset points',
#            arrowprops=dict(arrowstyle="->"))


t = ax.text(x_start+.85, y_start+1.35, "epsilon, ε", ha="center", va="center", rotation=40, size=15, **fig_font)

ax.set_xlim([-2.5,9.5])
ax.set_ylim([-3.5,7.5])
ax.legend(loc=1, prop=font)
plt.show()
