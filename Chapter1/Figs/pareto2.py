import sys
#import random as rand
import numpy as np
import random
import matplotlib.pyplot as plt

import matplotlib.font_manager as font_manager
#import matplotlib.font_manager as fm
fig_font = {'fontname':'Liberation Serif'}
font = font_manager.FontProperties(family='Liberation Serif')


def print2Dlist(twoDlist):
    print ("[")
    for line in twoDlist:
        print (line)
    print("]")

def pareto_front(twoDlist):
# from:https://math.stackexchange.com/questions/101125/how-to-compute-the-pareto-frontier-intuitively-speaking
#
# We say that an alternative A dominates B if A outscores B
# regardless of the tradeoff between value and cost that is,
# if A is both better and cheaper than B.
#
#Algorithm A:
#1. Let i:=1
#2. Add Ai to the Pareto frontier.
#3. Find smallest j>i such that value(Aj)>value(Ai)
#4. If no such j exists, stop. Otherwise let i:=j and repeat from step 2.
    i = 1
    for line in twoDlist:
        line.append(i)
        i+=1
    sortedList = sorted(twoDlist,key=lambda x:(x[0],x[1]))
    y = sortedList[0][1]
    #sortedList[0].append("pareto")
    for line in sortedList:
        if line[1] > y:
            line.append("dominated")
        else:
            line.append("pareto")
            y = line[1]
    pareto = []
    dominated = []
    for line in sortedList:
        if (line[3] == "pareto"):
            pareto.append(line)
        else:
            dominated.append(line)#[:2])
    print("pareto:")
    print2Dlist(pareto)
    #print2Dlist(dominated)
    return pareto, dominated


low = 0.0
high= 3.0
size= 45
np.random.seed(int(sys.argv[1])) # 6 is ok...
#randoms = [[random.uniform(low, high),random.uniform(low, high)] for i in xrange(size)]
randoms = []
for i in range(size):
    x_vals = np.random.normal(loc=5.0, scale=3.0, size=None)
    y_vals = np.random.normal(loc=5.0, scale=3.0, size=None)
#    xvals =[x_vals - y_vals * y_vals]
    xvals = x_vals - np.sqrt(y_vals)
    yvals = y_vals - np.sqrt(x_vals)
    randoms.append([[xvals],[yvals]])
pareto, dominated = pareto_front (randoms)
plt.xkcd()
#plt.rcParams['font.size'] = 12
#plt.rcParams['font.family'] = 'SedgwickAve-Regular.ttf'
ax = plt.gca()
# Change all the fonts to humor-sans.
#prop = fm.FontProperties(fname='SedgwickAve-Regular', size=16)
x = [row[1] for row in dominated]
y = [row[0] for row in dominated]
plt.scatter(x,y, color='g', alpha=0.6, marker='o')
x = [row[1] for row in pareto]
y = [row[0] for row in pareto]
#plt.plot(x,y, color='r', alpha=.65, marker='x', linestyle='-')
plt.step(x, y, color='r', alpha=0.65, where='pre', marker='x', linestyle='-')
plt.ylabel('objective y (to be minimised)', **fig_font)
plt.title("Demonstration of Pareto Fronts:", **fig_font)
plt.xlabel('objective x (to be minimised)', **fig_font)
for text in ax.texts:
    text.set_fontproperties(prop)
ax.set_xlim([-2.5,9.5])
ax.set_ylim([-3.5,7.5])
plt.show()
