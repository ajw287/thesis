%!TEX root = ../thesis.tex
%*******************************************************************************
%****************************** Third Chapter *********************************
%*******************************************************************************

%\chapter{Literature Review}\label{chap:litreview}
\chapter{Surrogate Model Optimisation, Deep Learning and PWR Fuel Management}\label{chap:litreview}
\ifpdf
    \graphicspath{{Chapter3/Figures/Raster/}{Chapter3/Figures/PDF/}{Chapter3/Figures/}}
\else
    \graphicspath{{Chapter3/Figures/Vector/}{Chapter3/Figures/}}
\fi

While the previous chapter introduced \gls{terms:smo} methods, this chapter examines the literature around in-core fuel management optimisation, \gls{terms:smo} and the growing trend for deep learning in nuclear engineering.

Nuclear science has a long history of pioneering modern numerical techniques.  For example, Monte Carlo \cite{ulam1961monte} and iterative optimisation \cite{parks1990intelligent}, but this would no longer appear to be the case. If the community continues to ignore modern techniques, it will inevitably struggle to attract talented scientists.

Until recently, there was almost no appetite for deep learning among the academic community in nuclear engineering.  The proceedings of PHYSOR 2018 \cite{PHYSOR2018} included no papers with the terms `machine learning', `deep learning', `neural network' or `surrogate model' in the title.  This bias has now been rightly discarded.  There is now much, perhaps too much, enthusiasm for deep learning in nuclear engineering, and a rapidly growing corpus of literature.  At the now cancelled PHYSOR 2020, there would have been eight papers presented that referenced `machine learning', `deep learning' or `neural networks' in their titles \cite{PHYSOR2020}.  The author is lucky to have worked at the forefront of these techniques, and has been able to contribute meaningfully to the adoption of deep learning techniques in \gls{terms:smo} and as a part of the wider nuclear engineering community.
%\section{Surrogate Model Optimisation Literature Survey}
\section{Applications of Surrogate Models}
%\subsection{SMO in Nuclear Engineering}
%\subsection{SMO in other Fields}
%\subsection{Uses for Surrogate Models}
%\tochide
The following sections concisely describe \gls{terms:smo} techniques that have been applied in other areas of engineering.
\subsection{Mechanical Engineering}
Examples of applying surrogate models in the area of mechanical engineering are concentrated in the area of \gls{terms:cfd}, but many other examples exist, including Yang \textit{et al.} \cite{yang2005metamodeling} who use surrogate models for vehicle frontal impact simulation, and Marklund \textit{et al.} \cite{marklund2001optimization}
who use first- and second-order polynomial approximations  to investigate impact on the `B-pillar' component of a car.  Forrester and Keane \cite{forrester2007multi} demonstrated the application of surrogate models to the design of a passive vibration isolating structure, creating novel performant structures, and later demonstrated helical spring design and cantilever `Nowacki' beam design by using surrogate modelling \cite{nowacki1980modelling},\cite[pp. 198--201]{9780470060681}.
%\tochide
\subsection{\Glsentrylong{terms:cfd} and Aerospace Design}
Perhaps the most successful application of surrogate approximation for the purposes of optimisation is in the calculation of flows around objects in fluid dynamics.  This is due to the high computational cost of performing \gls{terms:cfd} calculations, such as the direct numerical simulation of Navier-Stokes equations in three dimensions.  \gls{terms:cfd} lends itself to surrogate methods due to the validity of the assumptions of continuity and conservation laws, and the similarity of the shape of responses with common basis functions.

Aircraft wing optimisation is an extremely popular aerodynamic optimisation problem for surrogate modelling.  Robinson and Keane \cite{robinson2001concise} derived a family of \glspl{terms:rbf} for use as surrogate models when optimising supercritical aerofoil morphology, and Keane \cite{keane2003wing} designed wing morphology using \gls{terms:kriging} with multi-fidelity models in 2003.  Forrester \textit{et al.} \cite[p. 197]{9780470060681} demonstrate eighth-order \gls{terms:polynomialregression} to model drag coefficients on aerofoil design in 2008.  Research continues in 2013, when Li \textit{et al.} \cite{li2013aerodynamic} investigated the nacelle/pylon (engine) position on wings with \glspl{terms:rbf}.  Laurencau \cite{laurenceau2010comparison} compared advanced methods of \Gls{terms:kriging} effectiveness on aerodynamic shape optimisation, while Fincham and Friswell \cite{fincham2015aerodynamic} successfully applied \glspl{terms:rbf} to wing morphology in 2015.  Khurana \textit{et al.} \cite{khurana2009airfoil} investigated optimising aerofoil design using neural network surrogate models.  Recently, in 2016, Palar \textit{et al.} \cite{palar2016comparative} compared a number of multi objective optimisation algorithms while using a second-order local surrogate model, which is updated at each point in the search space.
\par
Helicopter rotor blades are another design problem commonly investigated using surrogate optimisation.  Collins \cite{collins2013application} applied fourth-order polynomial regression to rotor design, and Glaz \textit{et al.} \cite{glaz2009helicopter} used a \gls{terms:kriging} method to investigate vibration dampening in helicopter blades.  In addition Massaro \textit{et al.} \cite {massaro2011multiobjective} used a neural network with an evolutionary algorithm to optimise efficiency at multiple objective altitudes and speed conditions in 2011.
\par
Compressor design is another \gls{terms:cfd} problem that has benefitted from the application of surrogate models. Lian and Liou \cite{lian2005multiobjective} used second-order polynomial regression and Kipouros \textit{et al.} \cite{Kipouros2007} applied \gls{terms:rbf} models in the context of multi objective optimisation in compressor design.  Queipo \textit {et al.} \cite{queipo2005surrogate} applied polynomial surrogate models in the optimisation of the design of fuel injectors for NASA rockets in 2005.  In 2008, Jaeggi \textit{et al.} \cite{jaeggi2008robust} compared \gls{terms:kriging} with a related Gaussian process method called the \textit{Sparse Pseudo-input Gaussian Process} (SPGP) finding that the SPGP outperformed \gls{terms:kriging} on computation cost by a factor of four and produced better predictions in multi-fidelity models of diffuser design.
%\begin{quote}
%	\say{Neural networks (multi-layered radial basis functions) form a more sophisticated behavioural modelling technique which may be unnecessary for most aerodynamic optimisations.}\hspace*{\fill}\cite{SKINNER2018933}
%\end{quote}
%\tochide
\subsection{Meteorology and Physical Geography}
Weather prediction has long been used for the development of models and surrogate models.  Forrester \textit{et al.} note that meteorologists often work with surrogate models:
\begin{quote}
	\say{The pressure [contour] chart is, of course, a surrogate model of the real pressure distribution, based on measurements at a network of weather stations}\hspace*{\fill}\cite[p. 111]{9780470060681}
\end{quote}
Chorley \cite{chorley1965trend} reviewed a number of surrogate methods in use in physical geography in 1965, including linear and polynomial regression,  showing that this was already common practice at the time.
\par
%An excellent review of surrogate methods in relation to water resource modelling is Razavi \textit{et al.} \cite{razavi2012review}

Razavi \textit{et al.} \cite{razavi2012review} conducted and excellent review of surrogate methods related to water-resource modelling, covering polynomial regression, \glspl{terms:rbf}, and \gls{terms:kriging} methods, and referencing 48 papers on surrogate methods in water resources and many more over physical geography.  Their conclusion on the selection of methods is as follows:
\begin{quote} \label{quote:authorchoice}
	\say{It is not trivial to suggest the best function approximation technique for the purpose of response surface modeling, and metamodel developers typically pick a technique based on their preference and level of familiarity as well as software availability. Function approximation techniques that are able to (1) act as exact emulators, (2) provide a measure of approximation uncertainty, and (3) efficiently and effectively handle the size of the data set [...] of interest} \hspace*{\fill} \cite{razavi2012review}
\end{quote}

%\tochide
\subsection{Mineral Prospecting and Mining}
Building a mine or an oil well is an expensive undertaking.  Therefore, it is important in any geophysical survey to establish the minimum number of samples in order to make informed decisions.  However, extracting a core sample (a sample is denoted as $x$ in our description) is an activity that varies in cost -- from the collection of surface samples (which have negligible cost) to remote-area, \textit{ultra-deep}, offshore core samples, which are collected from kilometres down, where the specialised drilling rigs alone cost many millions of USD per week \cite{shafer2007coring}, and  there is significant danger to personnel \cite{christou2012safety}.
\par
For this reason, surrogate modelling methods have always been salient to geophysicists.  Most models use \gls{terms:gismap} map coordinates as the input, with a third dimension being the concentration of ore in samples.  \gls{terms:kriging} was invented by Danie Krige \cite[unsubstantiated]{krige1951statistical}, a South African mining engineer, and its history is described by Cressie \cite{Cressie1990}, who states that the term was coined by Matheron \cite{matheron1962traite} in 1962.
\par
As well as \gls{terms:kriging}, other methods have also been applied to geological problems, notably a method developed by Singer and Kouda \cite{singer1996application}, who successfully applied neural networks to predict of mineral deposits in Japan.  Research continues with further examples demonstrated by Porwal \textit{et al.} \cite{Porwal2003}  and  Corsini \textit{et al.} \cite{CORSINI200979}, who have both applied gls{terms:rbf} models to predict the location minerals and groundwater, respectively. 

\section{Surrogate Models in Nuclear Engineering}
The complexity of in-core fuel management and fuel design has long been recognised as being highly nonlinear. The US Atomic Energy Commission \cite[pp. 605--614]{usatomic1955reactor}, for example, discussed the nonlinear effects of control rods on adjacent fuel pins back in 1955. The in-core fuel management search space was investigated by Galperin \cite{doi:10.13182/NSE95-A24079} in 1995, who found the space to be extremely complex, with $10^{10}$ local optima in a search space  with $10^{12}$ states.
Despite the early recognition of the complexity of the problem, the label of surrogate modelling has not been used in the context of nuclear engineering until recently.   There have, however, been a few attempts at using surrogate models.  As early as 1993, Kim \textit{et al.} \cite{kim1993pressurized} used shallow (single, hidden layer) \glspl{terms:ann} to model PWR core parameters for a surrogate based optimisation of in-core fuel management.  They concluded that the accuracy of their predictions was too low for application to \gls{terms:smo}.  They were achieving between 5\% and 10\% prediction error in networks that were a few hundred neurons in size.  However, other works, such as Lysenko \textit{et al} \cite{lysenko1999neural} used neural networks to control parts of the calculation in concert with analytical techniques and demonstrated utility.  Another attempt was made by  Faria and Pereira \citep{FARIA2003603} in 2003, who again used smaller, shallow \glspl{terms:ann} for core-reload pattern design and optimisation.  This time, in-fill sampling was used to continually update the networks.  They found that they were able to consistently design better systems than the reference design.
They investigated the model using three different types of fuel preparation (MOX, AIROX and co-processing) but the experiment only considered the movement of fuel around the core rather than swapping fuel in and out.  In 2008, Bae et al. \cite{BAE20082200} used \gls{terms:svr}, kernel method is used to train \glspl{terms:ann} for prediction of \gls{nuclear:ppf} in fuel assemblies, concluding that the approach "sufficiently accurate for using in power peaking factor monitoring".  However, no optimisation methodology or control strategy is considered.
%  The experiment only considered moving fuel around in the core not changing any fuel in or out they investigated the model over three different reprocessing techniques: MOX, AIROX and Coprocessing.

Polyomial regression surrogate models such as the CESAR 5.3 code \cite{vidal2012cesar5} have been successfully applied in mean cross-section prediction over burnup.
%Polynomial regression surrogate models are successfully applied in mean cross-section prediction over burn up, for example in the CESAR 5.3 code \cite{vidal2012cesar5}

In recent years there has been an increasing trend of papers directly referencing \glspl{terms:metamodel}, meta-modelling or surrogate modelling applied to nuclear engineering problems.  In particular, Leniau \textit{et al.} \cite{LENIAU2015125} used shallow \glspl{terms:ann} to predict the plutonium content of MOX fuel, isotopic concentrations and cross-sections over burnup.
\begin{quote}
	\say{\glspl{terms:mlp} showed a very good accuracy since the average error on plutonium content determination is 0.37\% with a standard deviation associated to the error of 1.55\%.}\hspace*{\fill} \cite{LENIAU2015125}
\end{quote}
In 2018, Wu \textit{et al.} \cite{WU2018422} applied first-order \gls{terms:polynomialregression} and \gls{terms:kriging} to the BISON nuclear fuel simulation code for modelling the release of fission gases.  They found that \say{Kriging metamodels are applied to greatly alleviate the computer burden during [Markov Chain Monte Carlo] MCMC sampling.} \cite{WU2018422}
%The problem of nuclear fuel assembly design optimisation in lightwater reactors is to be investigated, with a view to creating a particular fuel design that is optimal for a given core reload.
\par
Despite the mixed results of previous studies using surrogate models to optimise in-core fuel management, and other aspects of nuclear engineering, it is clear that the problems can be tackled using this technique.  Furthermore, progress during the past ten years means that there is the possibility that further successes can be achieved by applying modern machine learning techniques to the area of fuel design and in-core fuel management.

\section{Deep Learning Surrogate Models}
%No application of \gls{terms:deepnetwork} \glspl{terms:ann} in fuel design or in-core fuel management had been located before the last twelve months,  
As mentioned previously, there is now a growing trend for the application of machine learning and particularly deep learning in the nuclear industry, based on methodological innovations such as \glspl{terms:cnn} \cite{LeCun2005} and availability of tools mentioned in section~\ref{sect:nntopology}.  For example, at the PHYSOR 2020 conference there would have been eight papers presented by other authors on `machine learning', or `neural networks' \cite{zhangPhysor2020, liPhysor2020, szamesPhysor2020, fiorinaPhysor2020, tanoPhysor2020, manringPhysor2020, radaidehPhysor2020, sobesPhysor2020}. Compared to the previous PHYSOR \cite{PHYSOR2018}, with no papers using these keywords in the title.

Of the PHYSOR 2020 papers, Zhang et al. \cite{zhangPhysor2020} refer to the use of \glspl{terms:cnn}, in this case, it is investigated for simulation of flux and power distribution compared to the diffusion equation, with a view to extending the work to optimisation. However, unlike this thesis the optimisation step is not achieved.


\subsection{Deep Learning Model Architecture}\label{sec:deep_learning}
The first type of neural network surrogate model used in this thesis is an MLP type -- a simple feedforward neural network commonly used for regression and classification tasks.  As shown in Figure \ref{fig:mlp}, an array of processing nodes connect inputs, $x$, to outputs, $y$; each node sums the inputs and applies a transform to the data; and between nodes a weighting factor $w_{i,j}$ is applied.  The weights, $w$, define an approximation of the required outputs, $y$.  The backpropagation algorithm, made popular by Rummelhart \textit{et al.}~\cite{Rummelhart1986}, is used to optimize the weights of the network.

The second type of deep learning surrogate model is called a \gls{terms:cnn} \cite{lecun1995convolutional}, represented diagrammatically in Figure \ref{fig:cnn}.  This network configuration is well-suited to spatial data, such as recognition of images \cite{kaggle_cats_and_dogs}.  In this architecture, a population of filter kernels perform `convolutions' on the input image.  The resulting images have a sensitivity to the pattern of the kernel that gave rise to them.  These images are usually put through a `pooling layer' (scaled down), and these layers are repeated a number of times.  The network finishes up with a deep feedforward neural network similar to an MLP.  The system is successful because it does not require the designer to specify the base `kernels' that will perform low-level image processing; they arise naturally from the convolution layers of the \gls{terms:cnn}.  This type of network has proven extremely effective at recognising images, due to its translational invariance.  Although translational invariance does not automatically occur in nuclear core design, by selecting inputs to represent the water gap and control/instrumentation pins, the effects of geometric variance are encoded into the inputs of the networks. The \gls{terms:cnn} represents an example of a typical advanced deep learning tool.

\begin{figure}[!htb]
	\centering
	\begin{subfigure}[b]{0.35\textwidth}
		\includegraphics[scale=0.60]{neural_network_mlp.pdf}
		\caption{A simplified diagram of an MLP.}
		\label{fig:mlp}  
	\end{subfigure}
	\hspace{0.5cm}           
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics[scale=0.325]{neural_network_cnn.pdf}
		\vspace{0.1cm}
		\caption{A diagram of the CNN layer structure.}   
		\label{fig:cnn}
	\end{subfigure}
	\caption[Deep learning topologies]{Deep learning topologies \cite{physor2020_microcore_figs}.}
	\label{fig:nn_archs}
\end{figure}

\section{PWR Fuel Design and Management}
Table \ref{table:fueloptliterature} shows a concise selection of the in-core fuel management literature in chronological order.  The earliest research found by the author was conducted by Haling, who proposed an optimal strategy for the power distribution of a BWR \cite{Haling1964}.  Although the optimisation strategy is simply heuristic, the work recognises that the subject is complex and of interest to operators.  Despite this early contribution,  very little research was conducted for the next twenty years, despite significant contributions from Naft and Sesonike \cite{Naft1971} and Ahn and Levine \cite{Ahn1985}, because the computational power for detailed neutronics simulation and iterative optimisation was not readily available.  As computer technology advanced in the 1980s, interest in approaches to iterative optimisation was revived.  Development of the UK AGR programme coincided with the increased availability of computational power, which made iterative optimisation approaches possible (e.g. \cite{Parks1988, parks1990intelligent}).  Arrangement of fuel is inherently a combinatorial problem.  Although iterative optimisation strategies cope well with combinatorial explosion, the search space is still extremely large and the number of iterations required makes them computationally expensive.  The implications of this were investigated by Galperin \cite{doi:10.13182/NSE95-A24079}, who concluded that since the computational power was not available at the time to conduct an iterative search, a heuristic approach was required to make the problem tractable.  Thus, two broad approaches were developed largely in parallel: either to iterate over solutions measuring the objective or to simplify the problem to the point where the objective is relatively easy to find.  

As computational power increased rapidly during the 1990s a large number of optimisation strategies have followed the stochastic optimisation approach.  A selection of these papers includes research by Mahlers \cite{MAHLERS1994223}, {\v{S}}muc \textit{et al.} \cite{vsmuc1994annealing}, DeChaine and Feltus \cite{dechaine1995nuclear}, Chapot \textit{et al.} \cite{chapot1999new}, and Francois and Lopez \cite{francois1999soprag}.  One of the main advantages of stochastic iterative optimisation is that it is possible when very little is known about the problem.  Heuristic approaches were championed by Galperin and his students, and hybrid techniques have also been investigated,  for example Stevens \textit{et al.} \cite{stevens1995optimization}.

Iterative optimisation does not guarantee the best solution (or non-dominated set), and is costly in terms of computational power.  However it has proven extremely robust and is thus able to deliver acceptable results for a wide range of problems.  The algorithms used -- typically simulated annealing and evolutionary algorithms -- have been successfully applied to a wide range of engineering problems \cite{9780470060681} and can be applied when no mathematical analysis of the system is possible.

On the other hand, heuristic approaches must be carefully designed to correspond with the real world.  An optimal solution for the heuristics does not guarantee a real-world solution and, inevitably, simplification must take place in order to create a tractable set of heuristic rules, which may mean that the optimisation is happening on the wrong problem.

\Gls{terms:smo} aims to unite these approaches by generating a model from analysis of the data and automating the heuristic rule generation.  It has the potential to be unbiased by human designers, and potentially avoids the heuristic approach's pitfall of optimising the wrong problem, while significantly reducing the computational load of the iterative optimisation algorithm.  Furthermore, with the advent of parallel computation techniques, it may be possible to develop the model in parallel with the evaluation of the objective function, essentially running a novel optimisation based on the new data produced at each objective evaluation.
%Researchers on both approaches usually look at different optimisation strategies or novel objectives. \cite{}

Although \Gls{terms:smo} is a relatively recent development in the nuclear field, a number of researchers have used \Gls{terms:smo}, without applying the label.  These include Faria and Pereira \cite{FARIA2003603} and Ortiz and Requena \cite{ortiz2004order} who have, in the past, applied neural networks as surrogate models to the problem of in-core fuel management.

In recent years, the nuclear academic community has begun to consider \Gls{terms:smo} and deep learning strategies more seriously, overcoming initial scepticism.  With the publication of review papers such as \cite{Nissan_2019} and \cite{gomez2020status}, we are seeing an acceptance of the significance of the innovations that have revolutionised subjects, such as natural language processing, image recognition and speech synthesis.  

This chapter has discussed the trends and techniques that are used for in-core fuel management, without investigating the tools that are required for the implementation of such a system.  The next chapter will introduce the tools that have been selected to develop an extensible framework for \gls{terms:smo} of in-core fuel management problems.
%\begin{landscape}
%\begin{table} [H]%[H]%
%%		\newgeometry{left=1.2cm}
%	 	\centering
% 		\label{table:fueloptliterature}
%	 	\begin{longtable}{p{2cm} m{6.5cm} c m{2cm} m{2cm} c c}
%			\caption{Selected literature on in-core fuel management} \\
%%			\hline
%%	 		\multicolumn{1}{c}{\textbf{Significant Authors}} & \multicolumn{1}{c}{\textbf{Title}} & \multicolumn{1}{c}{\textbf{Reactor Type} } & \multicolumn{1}{c}{\textbf{Algorithm} } & \multicolumn{1}{c}{\textbf{Method}} & \multicolumn{1}{c}{\textbf{{Date} } & \multicolumn{1}{c}{\textbf{Reference}}\\ 
%	 		\multicolumn{1}{c}{\textbf{Authors}} & \multicolumn{1}{c}{\textbf{Title}} &\multicolumn{1}{c}{\textbf{Reactor}}&\multicolumn{1}{c}{\textbf{Algorithm}}&\multicolumn{1}{c}{\textbf{Objective}}&\multicolumn{1}{c}{\textbf{Yr.}}&\multicolumn{1}{c}{\textbf{Ref.}}\\
%	 		\hline
%	 		\endfirsthead
%	 		\multicolumn{1}{c}{\textbf{Authors}} & \multicolumn{1}{c}{\textbf{Title}} &\multicolumn{1}{c}{\textbf{Reactor}}&\multicolumn{1}{c}{\textbf{Algorithm}}&\multicolumn{1}{c}{\textbf{Objective}}&\multicolumn{1}{c}{\textbf{Yr.}}&\multicolumn{1}{c}{\textbf{Ref.}}\\
%	 		\hline
%	 		\endhead
%	 		% all the lines above this will be repeated on every page
%			Haling, R K&Operating Strategy for Maintaining an Optimum Power Distribution Throughout Life&BWR&Analysis, Heuristics&1-D graphical method&1963&\cite{Haling1964}\\
%			
%			Fenech &Application of Optimisation Methods to Nuclear Power Plant Design and Optimisation&&&&1970&-$^a$\\
%	 		
%	 		Naft and Sesonike&A Direct Search Algorithm for Optimising Fuel Loading Patterns in PWRs&PWR&Direct Search&&1970&-$^a$\\
%	 		
%	 		Naft and Sesonike&Pressurized Water Reactor Optimal Fuel Management&PWR&Jumbo(direct search scheme)&Jumbo (simplified semi-analytic function)&1972&\cite{Naft1972}\\
%	 		
%	 		Ahn and Levine&Automatic Optimised Reload and Depletion method for Pressurised Water Reactor&PWR&&&1985&\cite{Ahn1985}\\
%	 		
%	 		Parks&Optimal in-core nuclear fuel cycles under integral constraints& AGR&Simulated Annealing&&1988&\\
%	 		
%	 		Parks&An Intelligent Stochastic Optimization Routine for Nuclear Fuel Cycle Design&AGR&Metropolis/ Simulated Annealing&&1991&\cite{Parks1990}\\
%	 		
%	 		Kropaczek \& Turinsky & In-Core Nuclear Fuel Management Optimization for Pressurized Water Reactors Utilizing Simulate Annealing & PWR & Simulated Annealing&&1995&\cite{KropaczekandTurinsky1995}\\
%	 		
%	 		Galperin&Exploration of the Search Space of the In-Core Fuel Management Problem by Knowledge-Base Techniques&PWR&tree search with Heurisitic Rules&unclear&1997&\cite{doi:10.13182/NSE95-A24079}\\
%	 		
%	 		Yamamoto&A Quantitative Comparison of Loading Pattern Optimization Methods for In-Core Fuel Management of PWR&PWR&Binary Exchange, Simulated Annealing, Genetic Algorithms&unclear&1997&\cite{doi:10.1080/18811248.1997.9733673}\\
%	 		
%	 		Poon and Parks&Application of genetic algorithms to in-core nuclear fuel management optimization&&Simulated Annealing, GPT, Genetic Algorithm&&1993&\cite{PoonandParks1993}\\
%	 		
%	 		Mahlers&Core loading pattern optimization for pressurized water reactors&PWR&Simulated Annealing Successive Linear Programming&&1994&\cite{MAHLERS1994223}\\
%	 		
%	 		
%	 		
%	 			
%	 		Faria and Pereira & Nuclear fuel loading pattern optimisation using a neural network&&&&2003& \cite{FARIA2003603}\\
%	 		
%	 		&&&&&&\\
%	 		&&&&&&\\
%	 		&&&&&&\\
%	 		\hline
%	 	\end{longtable}
%	  	\footnotesize{$^a$ Found in Naft\cite{} 1972 and others, original articles not located}
%%	  	\restoregeometry
%%	\end{table}
%\end{landscape}

%\begin{table} [H]%[H]%
%	 	\centering
% magic from longtable:http://www.ctex.org/documents/packages/table/longtable.pdf
\setlength\LTleft{-24pt}
%\setlength\LTright{-15pt}
\begin{longtable}{p{0.65cm} m{1cm} c c m{2.5cm} c c}
	\caption[Selected literature on in-core fuel management]{Selected literature on in-core fuel management.  Abbreviations used in this table: ACS=Ant Colony System, BE=Binary Exchange, GA=Genetic Algorithm, GPT=General Perturbation Theory, HR=Heuristic Rules  LP=Linear Programming, NN=Neural Network,   NC=Nodel Code, SA=Simulated Annealing} 
	\label{table:fueloptliterature}\\
	%			\hline
	\toprule
	\multicolumn{1}{c}{\textbf{First Author}} & \multicolumn{1}{c}{\textbf{Reactor}} & \multicolumn{1}{c}{\textbf{Algorithm}} & \multicolumn{1}{c}{\textbf{Evaluation}} &
	\multicolumn{1}{c}{\textbf{Software}} &
	\multicolumn{1}{c}{\textbf{Yr.}} & 
	\multicolumn{1}{c}{\textbf{Ref.}}\\
	\midrule
	\endfirsthead
	\toprule
	\multicolumn{1}{c}{\textbf{First Author}} & \multicolumn{1}{c}{\textbf{Reactor}} & \multicolumn{1}{c}{\textbf{Algorithm}} & \multicolumn{1}{c}{\textbf{Evaluation}} & 
	\multicolumn{1}{c}{\textbf{Software}} &
	\multicolumn{1}{c}{\textbf{Yr.}} & 
	\multicolumn{1}{c}{\textbf{Ref.}}\\
	\midrule
	\endhead			
	Haling &BWR&HR&1 group model&\multicolumn{1}{c}{-}&1963&\cite{Haling1964}\\
	Fenech &-&-&-&\multicolumn{1}{c}{-}&1970&-$^a$\\
	Naft & PWR&Direct Search&Semi-analytic&JUMBO&1971&\cite{Naft1971}\\
	%	 		Naft &PWR&Jumbo(direct search scheme)&Jumbo (simplified semi-analytic function)&1972&\cite{Naft1972}\\
	Ahn &PWR&-&-&\multicolumn{1}{c}{-}&1985&\cite{Ahn1985}\\
	%	 		Parks&AGR&SA&BNL Code&AMETROP&1988&\cite{parks1990intelligent}\\
	Parks&AGR&Metropolis/ SA&Custom code&AMETROP&1989&\cite{parks1990intelligent}\\
	Poon&-&SA, GA&GPT&FORMOSA&1993&\cite{PoonandParks1993}\\
	Mahlers&PWR&SA / LP&Green's method&\multicolumn{1}{c}{-}&1994&\cite{MAHLERS1994223}\\
	Kropaczek & PWR & SA&GPT&FORMOSA&1995&\cite{KropaczekandTurinsky1995}\\
	{\v{S}}muc&PWR&SA&1\textonehalf D Diffusion Eq.&MOCALPS&1995&\cite{vsmuc1994annealing}\\
	Stevens&PWR&SA&NC&SIMAN&1995&\cite{stevens1995optimization}\\
	DeChaine&PWR/ BWR&GA&NC&CIGARO&1995&\cite{dechaine1995nuclear}\\
	Parks &PWR&GA&GPT, NC&FORMOSA, PANTHER&1996 &\cite{parks1996multiobjective} \\
	Galperin&PWR&Tree search + HR&-&\multicolumn{1}{c}{-}&1997&\cite{doi:10.13182/NSE95-A24079}\\
	Yamamoto&PWR& GA, SA, GA+BE&diffusion&\multicolumn{1}{c}{-}&1997&\cite{doi:10.1080/18811248.1997.9733673}\\
	Chapot&PWR&GA&NC&GENESIS&1999&\cite{chapot1999new}\\
	Fran\c{c}ois&BWR&GA&NC&PRESTO-B&1999&\cite{francois1999soprag}\\ %GENESIS/ ALGER/ RECNOD
	
	Faria &PWR&NN&NC&WIMS, \newline CITATION&2003& \cite{FARIA2003603}\\
	Ortiz&BWR&GA+NN&-&CM-PRESTO&2004&\cite{ortiz2004order}\\
	Mart\'{i}n-del-Campo&BWR&GA&-&CM-PRESTO&2004&\cite{martin2004development}\\
	De Lima&PWR&ACS&-&RECNOD&2008&\cite{de2008nuclear}\\
	Esquivel-Estrada&BWR&ACS&&CM-PRESTO&2011&\cite{esquivel2011azcaxalli}\\
	Hill&PWR&tabu, SA, GA&GPT&FORMOSA-P&2015&\cite{Hill201564}
	%	 		&&&&&&\\
	%	 		&&&&&&\\
	%	 		&&&&&&\\
	%	 		\hline
	%	 	\end{longtable}
\end{longtable}
\vspace{-0.6cm}

\begin{footnotesize}
	\centering
	$^a$~Found in \cite{Naft1971} 1972 and others (original article by Fenech "Application of Optimization Methods
	to Nuclear Power Plant Design and Optimization, Trans.
	Am. Nucl. Soc., 13, 90 (1970)" not located).\\
	%\footnotesize{$^a$ Found in Naft\cite{Naft1971} 1972 and others, original article not located}
\end{footnotesize}
\vspace{0.75cm}
\clearpage
